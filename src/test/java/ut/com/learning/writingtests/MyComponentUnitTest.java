package ut.com.learning.writingtests;

import org.junit.Test;
import com.learning.writingtests.MyPluginComponent;
import com.learning.writingtests.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}