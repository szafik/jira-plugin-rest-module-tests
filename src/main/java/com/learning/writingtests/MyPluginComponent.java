package com.learning.writingtests;

public interface MyPluginComponent
{
    String getName();
}